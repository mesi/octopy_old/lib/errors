""" Just a start,
To think about: SECoP error class is defined in the documentation how to combine this with Octopus errors"""

def error_missing_mandatory(key, level='node'):
    print('KeyError, {} has to have a value at {} level'.format(key, level))


class SecopException(Exception):
    def __init__(self, message, **kwargs):
        self.lst = [str(self.__class__.__name__), str(message), str(kwargs)]
        super(SecopException, self).__init__(self.lst)

#    def __repr__(self):
    def __str__(self):
        return str(self.lst)

    @property
    def name(self):
        return self.__class__.__name__[:-len('Exception')]


class ProtocolErrorError(SecopException):
    """A malformed Request or on unspecified message was sent.
    This includes non-understood actions and malformed specifiers.
    Also if the message exceeds an implementation defined maximum size.
    note: this may be retryable if induced by a noisy connection. Still that should be fixed first!"""
    name = 'ProtocolError'
    pass


class NoSuchModuleError(SecopException):
    """The action can not be performed as the specified module is non-existent."""
    name = 'NoSuchModuleError'
    pass


class NoSuchParameterError(SecopException):
    """The action can not be performed as the specified parameter is non-existent."""
    name = 'NoSuchParameterError'
    pass


class NoSuchCommandError(SecopException):
    """The action can not be performed as the specified command is non-existent."""
    name = 'NoSuchParameterError'
    pass


class ReadOnlyError(SecopException):
    """The requested write can not be performed on a readonly value.."""
    name = 'ReadOnly'
    pass


class WrongTypeError(SecopException):
    """The requested parameter change or Command can not be performed as the argument has the wrong type.
    (i.e. a string where a number is expected.) It may also be used if an incomplete struct is sent,
    but a complete struct is expected."""
    name = 'WrongType'
    pass


class RangeError(SecopException):
    """The requested parameter change or Command can not be performed as the argument value is not in the allowed range
    specified by the datainfo property. This also happens if an unspecified Enum variant is tried to be used,
    the size of a Blob or String does not match the limits given in the descriptive data,
    or if the number of elements in an array does not match the limits given in the descriptive data."""
    name = 'RangeError'
    pass


class BadJSON(SecopException):
    """The data part of the message can not be parsed, i.e. the JSON-data is no valid JSON."""
    name = 'BadJSON'
    pass


class NotImplemented(SecopException):
    """A (not yet) implemented action or combination of action and specifer was requested.
    This should not be used in productive setups, but is very helpful during development."""
    name = 'NotImplemented'
    pass


class HardwareError(SecopException):
    """The connected hardware operates incorrect or may not operate at all due to errors inside or in connected
    components."""
    name = 'HardwareError'
    pass

#  _______________retryable errors____________________

class CommandRunningError(SecopException):
    name = 'CommandRunning'
    pass


class CommunicationFailedError(SecopException):
    name = 'CommunicationFailed'
    pass


class TimeoutError(SecopException):
    name = 'TimeoutError'
    pass


class IsBusyError(SecopException):
    name = 'IsBusy'
    pass


class IsErrorError(SecopException):
    name = 'IsError'
    pass


class DisabledError(SecopException):
    name = 'Disabled'
    pass


class ImpossibleError(SecopException):
    name = 'Impossible'
    pass


class ReadFailedError(SecopException):
    name = 'ReadFailed'
    pass


class OutOfRangeError(SecopException):
    name = 'OutOfRange'
    pass


class InternalError(SecopException):
    name = 'InternalError'
    pass


if __name__ == "__main__":
    try:
        raise RangeError('beskrivning')
    except RangeError as err:
        print(str(err))


